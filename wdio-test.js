describe('The website', function(){
  it('should display a button to submit an article on the homepage', function(){
      browser.url('/project');

      expect(browser.getTitle()).toBe('Minimal reproducible wdio-webpack-dev-server-service setup');
      expect(browser.element('h1').getText()).toBe('Minimal reproducible wdio-webpack-dev-server-service setup');
  });
});
