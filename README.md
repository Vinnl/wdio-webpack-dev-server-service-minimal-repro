This repository contains a minimal web app, with WebdriverIO and wdio-webpack-dev-server-service setup. It is meant to allow you to easily create a minimal reproduction of bugs you encounter, and thus assist you in reporting them.

Prerequisites: Node, Yarn and Firefox.

To get started, clone the repo and install the required dependencies:

    git clone git@gitlab.com:Vinnl/wdio-webpack-dev-server-service-minimal-repro.git
    cd wdio-webpack-dev-server-service-minimal-repro
    yarn

Then, run the tests and see whether they succeed:

    yarn run wdio

And that's it, basically! You can now modify the project to reproduce your bug, then push it somewhere and link it in your bug report. Happy testing!
